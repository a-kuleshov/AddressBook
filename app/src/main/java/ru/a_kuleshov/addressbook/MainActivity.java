package ru.a_kuleshov.addressbook;

import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBarDrawerToggle;
import android.view.Menu;
import android.view.View;
import android.widget.ImageButton;
import android.widget.Toast;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import Utills.Requester;
import ru.a_kuleshov.addressbook.Fragments.FormFragment;
import ru.a_kuleshov.addressbook.Fragments.HistoryItemsFragment;
import ru.a_kuleshov.addressbook.Fragments.HumanFragment;
import ru.a_kuleshov.addressbook.Fragments.HumansFragment;


public class MainActivity extends ActionBarActivity implements View.OnClickListener {
    private JSONArray humans_json;
    private Boolean hasNext;
    private int page;
    private List<Human> humans;
    private final DBHelper dbHelper = new DBHelper(this);
    private String firstName;
    private String lastName;
    private String patronymic;
    private DrawerLayout mDrawerLayout;
    private ActionBarDrawerToggle mDrawerToggle;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if (savedInstanceState == null) {
            getFragmentManager().beginTransaction()
                    .add(R.id.container, new FormFragment())
                    .commit();
        }

        ImageButton historyButton = (ImageButton) findViewById(R.id.left_menu_history);
        historyButton.setOnClickListener(this);
        ImageButton findHumanButton = (ImageButton) findViewById(R.id.menu__find_human);
        findHumanButton.setOnClickListener(this);

        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerToggle = new ActionBarDrawerToggle(
                this,                  /* host Activity */
                mDrawerLayout,         /* DrawerLayout object */
                R.string.open_map,  /* "open drawer" description for accessibility */
                R.string.open_map  /* "close drawer" description for accessibility */
        ) {
            public void onDrawerClosed(View view) { getSupportActionBar().setTitle("Close"); }
            public void onDrawerOpened(View drawerView) {
                getSupportActionBar().setTitle("Open");
            }
        };
        if (mDrawerLayout != null) {
            mDrawerLayout.setDrawerListener(mDrawerToggle);
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return false;
    }

    public Boolean hasNext() {
        return hasNext;
    }


    private long logToHistory (String firstName, String lastName, String patronymic) {
        SQLiteDatabase db = dbHelper.getWritableDatabase();

        db.delete(
                "History",
                "first_name = ? AND last_name = ? AND patronymic = ?",
                new String[]{firstName, lastName, patronymic}
        );
        ContentValues contentValues = new ContentValues();
        contentValues.put("first_name", firstName);
        contentValues.put("last_name", lastName);
        contentValues.put("patronymic", patronymic);
        long rowID = db.insert("History", null, contentValues);
        db.close();
        return rowID;
    }

    public void showListFragment(String firstName, String lastName, String patronymic)  {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        if (activeNetworkInfo == null || !activeNetworkInfo.isConnected() ) {
            warn(getString(R.string.no_internet));
            return;
        }
        this.page = 0;
        if (firstName == null || firstName.isEmpty()) {
            warn(getString(R.string.firstname_required));
            return;
        }
        if (lastName == null || lastName.isEmpty()) {
            warn(getString(R.string.lastname_required));
            return;
        }
        this.firstName = firstName;
        this.lastName = lastName;
        this.patronymic = patronymic;
        HumansLoader humansLoader = new HumansLoader();
        logToHistory(firstName,lastName, patronymic);

        ProgressDialog progress = new ProgressDialog(this);
        progress.setTitle(getString(R.string.loading));
        progress.setMessage(getString(R.string.please_wait));
        progress.show();

        humansLoader.execute(firstName, lastName, patronymic);
        humans = new ArrayList<Human>();
        JSONObject jsonObject = null;
        try {
            jsonObject = humansLoader.get();
            String status = jsonObject.getString("status");
            if (status.equals("OK")) {
                humans_json = jsonObject.getJSONArray("men");
                hasNext = jsonObject.getString("has_next").equals("1");
            } else {
                progress.dismiss();
                warn(getString(R.string.error_message));
                return;
            }

            for (int i = 0; i < humans_json.length(); i++) {
                JSONObject human_json = humans_json.getJSONObject(i);
                Human human = new Human(human_json);
                humans.add(human);
            }
        } catch (Exception e) {
            progress.dismiss();
            warn(getString(R.string.error_message));
            e.printStackTrace();
        }

        progress.dismiss();

        goToListStep();
    }

    private void warn(String text) {
        int duration = Toast.LENGTH_SHORT;
        Toast toast = Toast.makeText(this, text, duration);
        toast.show();
    }

    public List<Human> getHumans() {
        return humans;
    }

    void goToListStep() {
        FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.container, new HumansFragment());
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }

    void goToMainStep() {
        FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.container, new FormFragment());
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }

    void goToHistoryFragment() {
        FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
        HistoryItemsFragment historyItemsFragment = new HistoryItemsFragment();
        fragmentTransaction.replace(R.id.container, historyItemsFragment);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }

    public DBHelper getDBHelper(){
        return dbHelper;
    }

    public void showHumanFragment(Integer position, List<Human> humans) {
        if (this.humans == null) {
            this.humans = humans;
        }
        Human human = this.humans.get(position);
        FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
        HumanFragment humanFragment = HumanFragment.newInstance(human);
        fragmentTransaction.replace(R.id.container, humanFragment);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }

    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
    }

    public void loadMOAR(List<Human> humans) {
        if (humans != null && this.humans == null) {
            this.humans = humans;
        }
        HumansLoader humansLoader = new HumansLoader();
        page++;
        humansLoader.execute(firstName, lastName, patronymic, String.valueOf(page));
        JSONObject jsonObject = null;
        try {
            jsonObject = humansLoader.get();
            String status = jsonObject.getString("status");
            if (status.equals("OK")) {
                humans_json = jsonObject.getJSONArray("men");
                hasNext = jsonObject.getString("has_next").equals("1");
            }

            for (int i = 0; i < humans_json.length(); i++) {
                JSONObject human_json = humans_json.getJSONObject(i);
                Human human = new Human(human_json);
                this.humans.add(human);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id) {
            case R.id.menu__find_human:
                if (mDrawerLayout != null) {
                    mDrawerLayout.closeDrawer(GravityCompat.START);
                }
                goToMainStep();
                break;
            case R.id.left_menu_history:
                if (mDrawerLayout != null) {
                    mDrawerLayout.closeDrawer(GravityCompat.START);
                }
                goToHistoryFragment();
                break;
        }
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // Sync the toggle state after onRestoreInstanceState has occurred.
        if (mDrawerLayout != null ) {
            mDrawerToggle.syncState();
        }
    }

    private class HumansLoader extends AsyncTask<String,Void,JSONObject> {

        @Override
        protected JSONObject doInBackground(String... params) {

            String firstName = params[0];
            String lastName = params[1];
            String patronymic = null;
            String page = "0";

            int length = params.length;
            if (length > 2) {
                patronymic = params[2];
            }
            if (length > 3) {
                page = params[3];
            }


            Requester requester = new Requester();
            List<NameValuePair> httpParams = new ArrayList<NameValuePair>();
            httpParams.add(new BasicNameValuePair("first_name", firstName));
            httpParams.add(new BasicNameValuePair("last_name", lastName));
            if (patronymic != null) {
                httpParams.add(new BasicNameValuePair("patronymic", patronymic));
            }
            if ( page != null) {
                httpParams.add(new BasicNameValuePair("page", page));
            }
            String response = requester.get("http://addressbook.a-kuleshov.ru:9076", httpParams );

//            String response = "{\"has_next\":1,\"men\":[{\"street\":\"Крылатские холмы ул\",\"birthdate\":\"16.01.1984\",\"apartment\":\"439\",\"phone\":\"4131620\",\"last_name\":\"Кулешов\",\"patronymic\":\"Игоревич\",\"building\":\"2\",\"home\":\"39\",\"first_name\":\"Антон\"},{\"street\":\"Новикова-прибоя наб\",\"birthdate\":\"11.10.1989\",\"apartment\":\"71\",\"phone\":\"1922484\",\"last_name\":\"Кулешов\",\"patronymic\":\"Витальевич\",\"building\":\"1\",\"home\":\"12\",\"first_name\":\"Антон\"},{\"street\":\"Крылатские холмы ул\",\"birthdate\":\"16.01.1984\",\"apartment\":\"439\",\"phone\":\"4131620\",\"last_name\":\"Кулешов\",\"patronymic\":\"Игоревич\",\"building\":\"2\",\"home\":\"39\",\"first_name\":\"Антон\"},{\"street\":\"Будановой екатерины ул\",\"birthdate\":\"09.02.1990\",\"apartment\":\"59\",\"phone\":\"4177670\",\"last_name\":\"Кулешов\",\"patronymic\":\"Васильевич\",\"building\":\"\",\"home\":\"8\",\"first_name\":\"Антон\"},{\"street\":\"Юных ленинцев ул\",\"birthdate\":\"23.02.1994\",\"apartment\":\"34\",\"phone\":\"1751707\",\"last_name\":\"Кулешов\",\"patronymic\":\"Андреевич\",\"building\":\"1\",\"home\":\"68\",\"first_name\":\"Антон\"},{\"street\":\"Красноказарменная ул\",\"birthdate\":\"27.07.1986\",\"apartment\":\"11\",\"phone\":\"3623776\",\"last_name\":\"Кулешов\",\"patronymic\":\"Владимирович\",\"building\":\"2\",\"home\":\"12\",\"first_name\":\"Антон\"},{\"street\":\"Бирюлевская ул\",\"birthdate\":\"12.08.1983\",\"apartment\":\"143\",\"phone\":\"3270403\",\"last_name\":\"Кулешов\",\"patronymic\":\"Михайлович\",\"building\":\"\",\"home\":\"44/6\",\"first_name\":\"Антон\"},{\"street\":\"Охотничья ул\",\"birthdate\":\"17.04.1984\",\"apartment\":\"6\",\"phone\":\"9643151\",\"last_name\":\"Кулешов\",\"patronymic\":\"Александрович\",\"building\":\"6\",\"home\":\"10/12\",\"first_name\":\"Антон\"},{\"street\":\"Красноказарменная ул\",\"birthdate\":\"05.03.1985\",\"apartment\":\"11\",\"phone\":\"3623776\",\"last_name\":\"Кулешов\",\"patronymic\":\"Владимирович\",\"building\":\"2\",\"home\":\"12\",\"first_name\":\"Антон\"},{\"street\":\"Новоясеневский просп\",\"birthdate\":\"20.10.1985\",\"apartment\":\"281\",\"phone\":\"4218638\",\"last_name\":\"Кулешов\",\"patronymic\":\"Александрович\",\"building\":\"1\",\"home\":\"22\",\"first_name\":\"Антон\"}],\"status\":\"OK\"}";
            JSONObject jsonObject = null;

            if (response != null) {
                try {
                    jsonObject = new JSONObject(response);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            return jsonObject;
        }
    }

    @Override
    public void onBackPressed() {
        if (mDrawerLayout != null && mDrawerLayout.isDrawerOpen(GravityCompat.START)) {
            return;
        }
        FragmentManager fragmentManager = getFragmentManager();
        if (fragmentManager.getBackStackEntryCount() > 0){
            fragmentManager.popBackStackImmediate();
            fragmentManager.beginTransaction().commit();
        } else {
            super.onBackPressed();
        }
    }
}

