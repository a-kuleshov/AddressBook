package ru.a_kuleshov.addressbook.Fragments;

import android.app.Activity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;

import ru.a_kuleshov.addressbook.Human;
import ru.a_kuleshov.addressbook.MainActivity;
import ru.a_kuleshov.addressbook.R;

public class FormFragment extends Fragment implements View.OnClickListener, View.OnFocusChangeListener {
    private EditText firstName;
    private EditText lastName;
    private EditText patronim;


    public FormFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_form, container, false);
        firstName = (EditText) rootView.findViewById(R.id.edit_first_name);
        lastName = (EditText) rootView.findViewById(R .id.edit_last_name);
        patronim = (EditText) rootView.findViewById(R .id.edit_patronim);
        Button searchButton = (Button) rootView.findViewById(R.id.button_search);
        searchButton.setOnClickListener(this);

        rootView.findViewById(R.id.edit_patronim).setOnFocusChangeListener(this);
        rootView.findViewById(R.id.edit_last_name).setOnFocusChangeListener(this);
        rootView.findViewById(R.id.edit_first_name).setOnFocusChangeListener(this);
        return rootView;
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        MainActivity activity = (MainActivity) getActivity();
        switch (id) {
            case R.id.button_search:
                activity.showListFragment(firstName.getText().toString(), lastName.getText().toString(), patronim.getText().toString());

        }

    }


    @Override
    public void onFocusChange(View v, boolean hasFocus) {
//        if (!hasFocus) {
//            return;
//        }
//        LinearLayout linearLayout = (LinearLayout) getView();
//        ImageView imageView = (ImageView) linearLayout.findViewById(R.id.imageView);
//        linearLayout.removeView(imageView);
    }
}