package ru.a_kuleshov.addressbook.Fragments;


import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.util.List;

import ru.a_kuleshov.addressbook.Human;
import ru.a_kuleshov.addressbook.MainActivity;
import ru.a_kuleshov.addressbook.R;

/**
 * A simple {@link Fragment} subclass.
 *
 */
public class HumansFragment extends Fragment implements AbsListView.OnScrollListener {
    private List<Human> humans;
    private ListView humansListView;
    private Boolean isLoading = false;

    public HumansFragment() {
        setRetainInstance(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_humans, container, false);
        humansListView = (ListView) view.findViewById(R.id.list_view);
        MainActivity activity = (MainActivity) getActivity();
        if (humans == null) {
            humans = activity.getHumans();
        }
        MyHumanAdapter adapter = new MyHumanAdapter(activity,humans);
        humansListView.setAdapter(adapter);
        humansListView.setOnScrollListener(this);
        return view;
    }

    private void showOneHumanFragment(Integer position) {
        MainActivity activity = (MainActivity) getActivity();
        activity.showHumanFragment(position, humans);
    }

    @Override
    public void onScrollStateChanged(AbsListView view, int scrollState) {
    }

    @Override
    public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
        MyHumanAdapter myHumanAdapter = (MyHumanAdapter) view.getAdapter();
        if (isLoading) {
            return;
        }
        if (firstVisibleItem + visibleItemCount > totalItemCount - 5) {
            isLoading = true;
            MainActivity activity = (MainActivity) getActivity();
            Boolean hasNext = activity.hasNext();
            if (hasNext != null && hasNext) {
                myHumanAdapter.loadMore();
            }
//            else {
//                view.setOnScrollListener(null);
//            }
        }
    }

    public class MyHumanAdapter extends BaseAdapter implements View.OnClickListener  {
        final Context ctx;
        final LayoutInflater lInflater;
        final List<Human> objects;

        @Override
        public int getCount() {
            if (objects == null) return 0;
            return objects.size();
        }

        @Override
        public Object getItem(int position) {
            return objects.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View view = convertView;
            if ( view == null ) {
                view = lInflater.inflate(R.layout.human_item, parent, false);
            }

            Human item = (Human) getItem(position);

            view.setOnClickListener(this);
            TextView name = (TextView) view.findViewById(R.id.history_item__first_name);
            name.setText(item.fullName());
            ((TextView) view.findViewById(R.id.human_item__address) ).setText(item.getAddress());
            ((TextView) view.findViewById(R.id.human_item__birthday) ).setText(item.birthDate);
            view.setOnClickListener(this);

            view.setTag(position);
            return view;
        }

        MyHumanAdapter(Context context, List<Human> items) {
            ctx = context;
            objects = items;
            lInflater = (LayoutInflater) ctx
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        public void loadMore() {
            MainActivity activity = (MainActivity) getActivity();
            activity.loadMOAR(humans);
            humans = activity.getHumans();
            MyHumanAdapter adapter = (MyHumanAdapter) humansListView.getAdapter();
            adapter.notifyDataSetChanged();
            HumansFragment.this.isLoading = false;
            return;
        }

        @Override
        public void onClick(View v) {
            Integer position = (Integer) v.getTag();
            HumansFragment.this.showOneHumanFragment(position);
        }

    }




}
