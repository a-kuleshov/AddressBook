package ru.a_kuleshov.addressbook.Fragments;



import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

import ru.a_kuleshov.addressbook.DBHelper;
import ru.a_kuleshov.addressbook.HistoryAdapter;
import ru.a_kuleshov.addressbook.MainActivity;
import ru.a_kuleshov.addressbook.R;

/**
 * A simple {@link Fragment} subclass.
 *
 */
public class HistoryItemsFragment extends Fragment implements RecyclerView.OnItemTouchListener {
    private static final double THRESHOLD = 8; //mm
    private static final double DELETE_THRESHOLD = 30; //mm
    private static final double ANGLE = 1;
    private static final int INITIAL_COLOR = 0x88;
    private static final double INCH_TO_MM = 25.4;
    private List<MyHistoryItem> history;


    private Float firstX, firstY;
    private RelativeLayout childViewUnder;

    public HistoryItemsFragment() {
        // Required empty public constructor
    }

    private Boolean isBraked = false;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_history_items, container, false);
        RecyclerView recyclerView = (RecyclerView) rootView.findViewById(R.id.recycler_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        MainActivity activity = (MainActivity) getActivity();
        DBHelper dbHelper = activity.getDBHelper();
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        history = new ArrayList<MyHistoryItem>();
        Cursor c = db.query("History", null, null, null, null, null, null);
        if (c.moveToFirst()) {
            int nameColFirstName = c.getColumnIndex("first_name");
            int nameColLastName = c.getColumnIndex("last_name");
            int nameColPatronymic = c.getColumnIndex("patronymic");
            int nameColId = c.getColumnIndex("_id");
            do {
                MyHistoryItem myHistoryItem = new MyHistoryItem(
                        c.getInt(nameColId),
                        c.getString(nameColFirstName),
                        c.getString(nameColLastName),
                        c.getString(nameColPatronymic)
                );
                history.add(myHistoryItem);
            } while (c.moveToNext());
        }
        c.close();
        db.close();
        HistoryAdapter historyAdapter = new HistoryAdapter(history, (MainActivity) getActivity());
        recyclerView.setAdapter(historyAdapter);
        recyclerView.addOnItemTouchListener(this);
//        recyclerView.setOnClickListener(this);
        return rootView;
    }

    @Override
    public boolean onInterceptTouchEvent(RecyclerView recyclerView, MotionEvent motionEvent) {
        int action = motionEvent.getAction();
        switch (action) {
            case MotionEvent.ACTION_DOWN:
                firstX = motionEvent.getX();
                firstY = motionEvent.getY();
                break;
            case MotionEvent.ACTION_MOVE:
                return proceedMoveAction(motionEvent);
        }
        return false;
    }

    private void restoreColor() {
        if (childViewUnder == null) {
            return;
        }
        int red = INITIAL_COLOR ;
        int no_red = INITIAL_COLOR;
        int color = android.graphics.Color.rgb(red, no_red, no_red);
        LinearLayout decorator = (LinearLayout) childViewUnder.findViewById(R.id.decorator);
        ColorDrawable colorDrawable = (ColorDrawable) decorator.getBackground();
        colorDrawable.setColor(color);

    }

    private Boolean proceedMoveAction(MotionEvent motionEvent) {
        if (firstX == null || firstY == null) {
            return false;
        }
        Float deltaX = Math.abs(motionEvent.getX() - firstX);
        Float deltaY = Math.abs(motionEvent.getY() - firstY);
        Float density = getResources().getDisplayMetrics().xdpi;
        Double dist = Math.sqrt(
                Math.pow(deltaX, 2.0) + Math.pow(deltaY, 2.0)
        )*INCH_TO_MM/density;
        return dist > THRESHOLD && deltaY/deltaX < ANGLE;
    }

    @Override
    public void onTouchEvent(RecyclerView recyclerView, MotionEvent motionEvent) {
        int action = motionEvent.getAction();
        if (action == MotionEvent.ACTION_OUTSIDE || action == MotionEvent.ACTION_CANCEL || action == MotionEvent.ACTION_UP) {
            restoreColor();
            firstX = null;
            firstY = null;
            isBraked = false;
            return;
        }
        if (isBraked) {
            return;
        }
        Float density = getResources().getDisplayMetrics().xdpi;
        double deltaX = Math.abs(motionEvent.getX() - firstX)*INCH_TO_MM/density;
        childViewUnder = (RelativeLayout) recyclerView.findChildViewUnder(firstX, firstY);
        if (childViewUnder == null) {
            return;
        }
        LinearLayout decorator = (LinearLayout) childViewUnder.findViewById(R.id.decorator);
        ColorDrawable colorDrawable = (ColorDrawable) decorator.getBackground();

        double k = deltaX/DELETE_THRESHOLD;
        if (k >= 1) {
            isBraked = true;
            int position = recyclerView.getChildPosition(childViewUnder);
            removeItemFromDB(history.get(position).id);
            history.remove(position);
            recyclerView.getAdapter().notifyItemRemoved(position);
            HistoryAdapter historyAdapter = (HistoryAdapter) recyclerView.getAdapter();
            historyAdapter.addDeleted(position);
        }
        int red = (int) (INITIAL_COLOR + (0xFF - INITIAL_COLOR)*k);
        int no_red = (int) (INITIAL_COLOR*(1-k));
        int color = android.graphics.Color.rgb(red, no_red, no_red);
        colorDrawable.setColor(color);

    }

    private void removeItemFromDB(int position) {
        MainActivity activity = (MainActivity) getActivity();
        DBHelper dbHelper = activity.getDBHelper();
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        String[] whereArgs = {String.valueOf(position)};
        db.delete("History", "_id=?", whereArgs);
    }

    public class MyHistoryItem  {
        final int id;
        public final String firstName;
        public final String lastName;
        public final String patronymic;

        public MyHistoryItem(int id, String firstName, String lastName, String patronymic) {
            this.id = id;
            this.firstName = firstName;
            this.lastName = lastName;
            this.patronymic = patronymic;
        }

        public String getFullName() {
            String fullName = "";
            if (firstName != null) {
                fullName += firstName.concat(" ");
            }
            if (lastName != null) {
                fullName += lastName.concat(" ");
            }
            if (patronymic != null) {
                fullName += patronymic;
            }
            return fullName;
        }
    }

}
