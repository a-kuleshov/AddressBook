package ru.a_kuleshov.addressbook.Fragments;

import android.app.AlertDialog;
import android.app.Fragment;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import ru.a_kuleshov.addressbook.Human;
import ru.a_kuleshov.addressbook.R;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link HumanFragment#newInstance} factory method to
 * create an instance of this fragment.
 *
 */
public class HumanFragment extends Fragment implements View.OnClickListener {
    private Human human;
    private EditText inputDialog;
    private final DialogInterface.OnClickListener dialogOnClick = new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialog, int which) {
            if (which == -1) {
                String phone = inputDialog.getText().toString();
                String uri = "tel:" + phone.trim() ;
                Intent intent = new Intent(Intent.ACTION_CALL);
                intent.setData(Uri.parse(uri));
                startActivity(intent);
            }
        }
    };

    public static HumanFragment newInstance(Human human) {
        HumanFragment fragment = new HumanFragment();
        Bundle args = new Bundle();
        args.putParcelable("human",human);
        fragment.setArguments(args);
        return fragment;
    }
    public HumanFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            human = getArguments().getParcelable("human");
        }
    }


    public void onSaveInstanceState(Bundle outState) {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View rootView = inflater.inflate(R.layout.fragment_human, container, false);
        ( (TextView) rootView.findViewById(R.id.first_name)).setText(human.firstName + " " + human.patronymic);
        ( (TextView) rootView.findViewById(R.id.last_name)).setText(human.lastName);
        ( (TextView) rootView.findViewById(R.id.phone)).setText(human.phone);
        ( (TextView) rootView.findViewById(R.id.address)).setText(human.getAddress());
        ( (TextView) rootView.findViewById(R.id.birthday)).setText(human.birthDate);
        ( (ImageButton) rootView.findViewById(R.id.button_add_to_contacts) ).setOnClickListener(this);
        ( (ImageButton) rootView.findViewById(R.id.button_open_map) ).setOnClickListener(this);
        ( (ImageButton) rootView.findViewById(R.id.findVK) ).setOnClickListener(this);
        ( (TextView) rootView.findViewById(R.id.phone) ).setOnClickListener(this);

        ImageButton callButton = (ImageButton) rootView.findViewById(R.id.button_call);
        if (callButton != null) {
            callButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    TextView textView = (TextView) rootView.findViewById(R.id.phone);
                    phoneDialog(textView.getText().toString().trim());
                }
            });
        }
        return rootView;
    }


    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id) {
            case R.id.phone:
                phoneDialog(((TextView) v).getText().toString().trim());
                break;
            case R.id.button_add_to_contacts:
                addToContacts();
                break;
            case R.id.button_open_map:
                openMap();
                break;
            case R.id.findVK:
                openVK();
                break;
            default:
                break;
        }
    }

    private void openVK() {
        String birthday = human.birthDate;
        String[] strings = birthday.split("\\.");
        String day = strings[0];
        String month = strings[1];
        String year = strings[2];
        String vkString = "http://vk.com/search?[section]=people&c[bday]=" + day +
                "&c[byear]=" + year +
                "&c[bmonth]=" + month +
                "&c[q]=" + human.firstName + " " + human.lastName;
        Log.i("vkString", vkString);
        Intent myIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(vkString));
        startActivity(myIntent);
    }

    private void openMap() {
        Uri uri = Uri.parse("geo:0,0?q=" + human.getAddress());
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setData(uri);
        startActivity(intent);

    }

    void phoneDialog(String phone) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        inputDialog = new EditText(getActivity());
        inputDialog.setText(phone);
        builder.setPositiveButton(getString(R.string.phone_dialog__yes), dialogOnClick);
        builder.setNegativeButton(getString(R.string.phone_dialog__no), dialogOnClick);
        builder.setView(inputDialog);



        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }


    private void addToContacts() {
        Intent intent = new Intent(Intent.ACTION_INSERT);
        intent.setType(ContactsContract.Contacts.CONTENT_TYPE);
        intent.putExtra(ContactsContract.Intents.Insert.NAME, human.firstName);
        intent.putExtra(ContactsContract.Intents.Insert.FULL_MODE, human.firstName);

        intent.putExtra(ContactsContract.Intents.Insert.PHONETIC_NAME, human.lastName);
        intent.putExtra(ContactsContract.Intents.Insert.POSTAL, human.getAddress());
        startActivity(intent);
    }


}
