package ru.a_kuleshov.addressbook;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import org.json.JSONException;

import java.util.AbstractList;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import ru.a_kuleshov.addressbook.Fragments.HistoryItemsFragment;

/**
 * Created by anton on 04.12.14.
 */
public class HistoryAdapter extends RecyclerView.Adapter<HistoryAdapter.ViewHolder> implements View.OnClickListener {
    private final MainActivity activity;
    private final AbstractList<HistoryItemsFragment.MyHistoryItem> items;
    private List<Integer> removedList = new ArrayList<Integer>();

    public HistoryAdapter(List<HistoryItemsFragment.MyHistoryItem> history, MainActivity activity) {
        items = (AbstractList<HistoryItemsFragment.MyHistoryItem>) history;
        this.activity = activity;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.history_item, null);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int i) {
        HistoryItemsFragment.MyHistoryItem item = items.get(i);
        viewHolder.itemView.setTag(item);
        viewHolder.textView.setText(item.getFullName());
        viewHolder.itemView.setTag(R.id.huemoe, i);
        viewHolder.itemView.setOnClickListener(this);
    }


    @Override
    public int getItemCount() {
        return items.size();
    }

    @Override
    public void onClick(View v) {
        int pos = (int) v.getTag(R.id.huemoe);
        Iterator<Integer> iterator = removedList.iterator();
        int minus = 0;
        while (iterator.hasNext()) {
            if (iterator.next() < pos) {
                minus++;
            }
        }
        HistoryItemsFragment.MyHistoryItem item = items.get(pos-minus);
        activity.showListFragment(item.firstName, item.lastName, item.patronymic);

    }

    public void addDeleted(int position) {
        removedList.add(position);
    }

    public static class ViewHolder extends RecyclerView.ViewHolder  {
        final TextView textView;

        public ViewHolder(View v) {
            super(v);
            textView = (TextView) v.findViewById(R.id.history_item__first_name);
        }
    }

    public void removeItem(int position) {
        items.remove(position);
        this.notifyItemRemoved(position);
    }
}
