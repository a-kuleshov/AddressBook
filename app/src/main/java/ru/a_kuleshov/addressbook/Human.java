package ru.a_kuleshov.addressbook;

import android.os.Parcel;
import android.os.Parcelable;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by anton on 27.10.14.
 */
public class Human implements Parcelable {
    public String firstName;
    public String lastName;
    public String patronymic;
    public String birthDate;
    private String street;
    private String apartment;
    public String phone;
    private String building;
    private String home;

    public Human(String firstName, String lastName, String patronymic, String birthDate, String street, String apartment, String phone, String building, String home) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.patronymic = patronymic;
        this.birthDate = birthDate;
        this.street = street;
        this.apartment = apartment;
        this.phone = phone;
        this.building = building;
        this.home = home;
    }

    public Human(JSONObject human_json) throws JSONException {
        this.firstName = human_json.getString("first_name");
        this.lastName = human_json.getString("last_name");
        this.patronymic = human_json.getString("patronymic");
        this.birthDate = human_json.getString("birthdate");
        this.street = human_json.getString("street");
        this.apartment = human_json.getString("apartment");
        this.phone = human_json.getString("phone");
        this.building = human_json.getString("building");
        this.home = human_json.getString("home");
    }

    public String fullName() {
        return firstName.concat(" ").concat(patronymic);
    }

    public String getAddress() {
        return street.concat(", д. ").concat(home).concat(", кв. ").concat(apartment);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(firstName);
        dest.writeString(lastName);
        dest.writeString(patronymic);
        dest.writeString(birthDate);
        dest.writeString(street);
        dest.writeString(apartment);
        dest.writeString(phone);
        dest.writeString(building);
        dest.writeString(home);
    }

    public static final Parcelable.Creator<Human> CREATOR = new Parcelable.Creator<Human>() {
        public Human createFromParcel(Parcel in) {
            return new Human(in);
        }

        @Override
        public Human[] newArray(int size) {
            return new Human[size];
        }
    };

    private Human(Parcel parcel) {
        firstName = parcel.readString();
        lastName = parcel.readString();
        patronymic = parcel.readString();
        birthDate = parcel.readString();
        street = parcel.readString();
        apartment = parcel.readString();
        phone = parcel.readString();
        building = parcel.readString();
        home = parcel.readString();
    }
}
