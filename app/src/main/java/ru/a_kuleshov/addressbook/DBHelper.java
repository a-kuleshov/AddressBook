package ru.a_kuleshov.addressbook;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

/**
 * Created by anton on 07.09.14.
 */
public class DBHelper extends SQLiteOpenHelper {

    private final String createString = "CREATE TABLE History (\n" +
            "  _id INTEGER PRIMARY KEY,\n" +
            "  first_name VARCHAR(45) NULL,\n" +
            "  last_name VARCHAR(45) NULL,\n" +
            "  patronymic VARCHAR(45) NULL,\n" +
            "  time TIMESTAMP NULL);\n";

    public DBHelper(Context context) {
        super(context, "myDB", null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        Log.i("op:", "--- onCreate database ---");
        db.execSQL(createString);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        String dropString = "drop table History";
        db.execSQL(dropString);
        db.execSQL(createString);
    }

}
